var express = require('express');
var router = express.Router();

// send email
var mailer = require('../public/assets/js/mailer/mailer');
router.post('/send_mail', function (req, res) {

      //console.log("in send_mail: req.body", req.body);

      if (! req.body['message']) {
          res.status(400).send('Can not send without message!')
          return;
      }
      
      if (! req.body['email']) {
          res.status(400).send('Can not send without email!')
          return;
      }
      
      if (! req.body['name']) {
          res.status(400).send('Can not send without name!')
          return;
      }
      

       mailer.sendEmail(req.body['message'], req.body['email'], req.body['name']);

       var response = {
            status  : 200,
            success : 'Email sent successfully'
        }
        res.end(JSON.stringify(response));

});


router.get('/platform', function(req,res,next){
    res.render('platform');
})

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index');
});



module.exports = router;
