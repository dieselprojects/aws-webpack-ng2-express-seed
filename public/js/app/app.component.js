import { Component, ElementRef } from '@angular/core';
export var AppComponent = (function () {
    function AppComponent(elementRef) {
        this.elementRef = elementRef;
        var native = this.elementRef.nativeElement;
        this.data = native.getAttribute("data");
        console.log("in c");
        console.log(this.data);
    }
    AppComponent.decorators = [
        { type: Component, args: [{
                    selector: 'my-app',
                    templateUrl: './app.component.html',
                    styleUrls: ['./app.component.css']
                },] },
    ];
    /** @nocollapse */
    AppComponent.ctorParameters = [
        { type: ElementRef, },
    ];
    return AppComponent;
}());
